package Model;

import java.sql.Date;
import java.util.ArrayList;

public class Profile {
	
	 public String id;
	 public String id_collect;
	 public String link;
	 public String naturalidade;
	 public String moradia;
	 public String data_nascimento;
	 public String relacionamento;
	 public String interesse;
	 public String trabalho;
	 public String ensino;
	 public String genero;
	 public String religiao;
	 public String idiomas;
	 public String data_coleta;
	 public ArrayList<Likes> likes = new ArrayList<Likes>();
	 public ArrayList<Book> books = new ArrayList<Book>();
	 public ArrayList<Checkin> checkins = new ArrayList<Checkin>();
	 public ArrayList<Familia> membrosFamilia = new ArrayList<Familia>();
	 public ArrayList<Game> games = new ArrayList<Game>();
	 public ArrayList<Movie> movies = new ArrayList<Movie>();
	 public ArrayList<Music> musics= new ArrayList<Music>();
	 public ArrayList<Photo> photos = new ArrayList<Photo>();
	 public ArrayList<Sport> sports = new ArrayList<Sport>();
	 public ArrayList<Tv> tvs = new ArrayList<Tv>();
	 public ArrayList<Acontecimento> acontecimentos = new ArrayList<Acontecimento>();
	 public ArrayList<Athlete> athletes = new ArrayList<Athlete>();
	 
}
