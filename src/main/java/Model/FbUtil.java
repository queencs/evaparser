package Model;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FbUtil {
	public static LocalDate textToDate(String text) throws ParseException {
		if (text.length() <= 6) {
			return LocalDate.now();
		} else if (text.contains("Ontem")){
			return LocalDate.now().minusDays(1);
		} else {
		
			
			String day = text.substring(0, 2).trim();
			day = day.length() == 1 ? new StringBuffer(day).insert(0, "0").toString() : day;
			
			String month = "";
			
			if (text.contains("jan"))
				month = "01";
			else if (text.contains("fev"))
				month = "02";
			else if (text.contains("mar"))
				month = "03";
			else if (text.contains("abr"))
				month = "04";
			else if (text.contains("mai"))
				month = "05";
			else if (text.contains("jun"))
				month = "06";
			else if (text.contains("jul"))
				month = "07";
			else if (text.contains("ago"))
				month = "08";
			else if (text.contains("set"))
				month = "09";
			else if (text.contains("out"))
				month = "10";
			else if (text.contains("nov"))
				month = "11";
			else if (text.contains("dez"))
				month = "12";
			
			String year = "";
			Matcher yearPattern = Pattern.compile(".*(\\d{4}).*").matcher(text);
			if (yearPattern.find())
				year = yearPattern.group(1);
			else
				year = String.valueOf(LocalDate.now().getYear());
			
			String date = day + "-" + month + "-" + year;
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			return LocalDate.parse(date, fmt);
			
			
		}
	}

}
