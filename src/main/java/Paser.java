import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.google.gson.Gson;


import Model.Acontecimento;
import Model.Athlete;
import Model.Book;
import Model.Checkin;
import Model.Familia;
import Model.Game;
import Model.Likes;
import Model.Movie;
import Model.Music;
import Model.Photo;
import Model.Profile;
import Util.InsertEvaCollect;
import Model.Sport;
import Model.Tv;
import Model.FbUtil;


public class Paser {

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InterruptedException, ParseException {

		//FileReader arquivo = new FileReader("C:\\Users\\Administrator\\Desktop\\all.txt");

		FileReader arquivo = new FileReader("C:\\Users\\USER\\Desktop\\all.txt");

		Scanner Input = new Scanner(System.in);

		System.out.println("ID Da Coleta");
		//String IdColeta = Input.next();
		String IdColeta ="654545";
		System.out.println("Data da Coleta <AAAA-MM-DD>");
		//String DataColeta = Input.next();
		String DataColeta = "2018-01-00";



		BufferedReader leitor = new BufferedReader(arquivo);
		String linha = leitor.readLine();
		Profile profile  = null;
		InsertEvaCollect profiles = new InsertEvaCollect();
		Likes likes = new Likes();
		Music music = null;
		Book book = null;
		Game game = null;
		Movie movie = null;
		Sport sport = null;
		Athlete atleta = null;
		Checkin checkin = null;
		Tv tv = null;
		Photo photo = null;
		Familia familia = null;
		Acontecimento acontecimento = null;
		BufferedWriter conexao = null;
		FileWriter arquivo2 = null;
		String jsonWords = null;

		while(linha !=null)
		{
			if(linha.trim().equals("//--NEW PROFILE--//"))
			{
				linha =leitor.readLine();
				profile = new Profile();
				checkin = new Checkin();

				while(linha !=null && !linha.equals("//--NEW PROFILE--//"))
				{

					if(linha.trim().equals("Visão Geral:"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM VISAO GERAL"))
						{
							profile.link = ""; // (linha.trim().split("ID: ")[1]);
							linha = leitor.readLine();
							profile.link = (linha.trim().split("Link: ")[1]);
							linha = leitor.readLine();
							profile.id_collect = IdColeta;
							profile.data_coleta = DataColeta;

							System.out.println("IdColeta: "+profile.id_collect);
							System.out.println("Data Coleta: "+profile.data_coleta);

							profile.trabalho = (linha.trim()).replace("'", "");
							linha = leitor.readLine();
							if(linha.trim().split(":")[0].equals("Anterior"))
							{
								linha = leitor.readLine();
							}


							profile.ensino = (linha.trim()).replace("'", "");
							if(!linha.trim().equals("FIM VISAO GERAL"))
							{
								linha =leitor.readLine();
							}

							if(linha.trim().split(":")[0].equals("Anterior"))
							{
								linha = leitor.readLine();
							}

							if(linha.trim().split(" ")[0].equals("Mora"))
							{
								profile.moradia = (linha.trim().split("Mora em ")[1]).replace("'", "");
								linha =leitor.readLine();
							}


							if(linha.trim().split(" ")[0].equals("De"))
							{
								profile.naturalidade = (linha.trim().split("De ")[1].split("· ")[0]).replace("'", "");
								linha =leitor.readLine();
							}

							while(!linha.trim().equals("FIM VISAO GERAL"))
							{
								linha = leitor.readLine();
							}

						}
						System.out.println("Terminou Visao Geral");
					}
					else if(linha.trim().contains("INFORMAÇÕES BÁSICAS E DE CONTATO:"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM INFORMAÇÕES BÁSICAS E DE CONTATO"))
						{
							linha = leitor.readLine();
							if(linha.trim().equals("Data de nascimento"))
							{
								linha = leitor.readLine();
								profile.data_nascimento = linha.trim().replace("'", "");
								linha = leitor.readLine();
							}
							if(linha.trim().equals("Gênero"))
							{
								linha = leitor.readLine();
								profile.genero = linha.trim().replace("'", "");
								linha = leitor.readLine();
							}
							if(linha.trim().equals("Interesses"))
							{
								linha = leitor.readLine();
								profile.interesse = linha.trim().replace("'", "");
								linha = leitor.readLine();
							}
							if(linha.trim().equals("Idiomas"))
							{
								linha = leitor.readLine();
								profile.idiomas = linha.trim().replace("'", "");
								linha = leitor.readLine();
							}
							if(linha.trim().equals("Religião"))
							{
								linha = leitor.readLine();
								profile.religiao = linha.trim().replace("'", "");
								linha = leitor.readLine();
							}
						}
						System.out.println("Terminou Info");
					}
					else if(linha.trim().equals("FAMILIA"))
					{
						linha = leitor.readLine();
						if(linha.trim().equals("INFORMAÇÕES DE CONTATO"))
						{
							while(linha.trim().equals("FIM FAMILIA"))
							{
								linha = leitor.readLine();
							}
							if(!linha.trim().equals("FIM FAMILIA"))
							{
								linha = "FIM FAMILIA";
							}

						}
						System.out.println("AQUI: "+profile.link);
						//System.out.println(profile.link);
						while(!linha.trim().equals("FIM FAMILIA"))
						{
							if(linha.trim().equals("RELACIONAMENTO"))
							{
								linha = leitor.readLine();

								if(!linha.trim().equals("Nenhuma informação de relacionamento a ser exibida"))
								{
									String tipo_relacionamento = (linha.trim()).replace("'", "");
									linha = leitor.readLine();
									if(!linha.trim().equals("MEMBROS DA FAMÍLIA"))
									{
										profile.relacionamento = linha.trim().replace("'", "");
										linha = leitor.readLine();
									}
									else
									{
										profile.relacionamento = tipo_relacionamento;
										linha = leitor.readLine();
									}

								}
								else
								{
									System.out.println("Tipo: Relacionamento não declarado.");
									linha = leitor.readLine();
								}
							}
							if(linha.trim().equals("MEMBROS DA FAMÍLIA"))
							{
								linha = leitor.readLine();

							}


							if(!linha.trim().equals("Nenhum membro da família a ser exibido"))
							{
								linha = leitor.readLine();
								linha = leitor.readLine();
							}
							else
							{
								linha = leitor.readLine();
							}


						}
						linha = leitor.readLine();
						//System.out.println(linha);
						System.out.println("Terminou Familia");
					}
					else if(linha.trim().equals("TV"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM TV"))
						{
							List<String> tvs_local = Arrays.asList(linha.split("/<>/"));
							for (String tv_local: tvs_local)
							{
								tv = new Tv();
								//String aux = "";
								tv.tv = (tv_local.split("TV:")[1]).replace("'", "");

								//tv.tv = (aux.split("Link:")[0]).replace("'", "");
								//System.out.println("Tipo TV:"+aux);
								//tv.link = aux.split("Link:")[1];
								//System.out.println(tv.link);
								tv.id_profile = profile.link;
								tv.data_coleta = profile.data_coleta;


								profile.tvs.add(tv);
							}
							linha = leitor.readLine();
						}
						System.out.println("Terminou TV");

					}else if(linha.trim().equals("CHECKINS"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM CHECKINS"))
						{
							if(!linha.trim().equals(""))
							{
								//System.out.println(profile.link);
								//System.out.println(linha);
								checkin = new Checkin();
								//System.out.println(linha);
								checkin.local = (linha.trim().split("Checkin:")[1]).replaceAll("'", "");
								linha = leitor.readLine();
								checkin.link = (linha.trim().split("Link:")[1]).replaceAll("'", "");
								checkin.id_profile = profile.link;
								checkin.data_coleta = profile.data_coleta;


								linha = leitor.readLine();
								if(linha.trim().split(":")[0].equals("Data"))
								{
									if(linha.trim().contains("Visitou em"))
									{
										checkin.data = FbUtil.textToDate(linha.trim().split(":")[1].split("Visitou em ")[1].toString());
									}
									if(linha.trim().contains("há cerca de"))
									{
										checkin.data = FbUtil.textToDate(linha.trim().split(":")[1].split("há cerca de ")[1].toString());
									}

								}
								linha = leitor.readLine();
								profile.checkins.add(checkin);
							}
						}
						System.out.println("Terminou Checkin");
					}
					else if(linha.trim().equals("SPORTS"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM SPORTS"))
						{
							List<String> sports_local = Arrays.asList(linha.split("/<>/"));
							for (String sport_local : sports_local)
							{
								sport = new Sport();
								//String aux = "";
								sport.sport = (sport_local.split("Sport:")[1]).replace("'", "");
								//sport.sport = aux;
								//sport.sport = aux.split("Link:")[0];
								//System.out.println("Tipo Sport:"+sport.sport);
								//sport.link = aux.split("Link:")[1];s
								sport.id_profile = profile.link;
								sport.data_coleta = profile.data_coleta;

								profile.sports.add(sport);
							}
							linha = leitor.readLine();
						}
						System.out.println("Terminou Sport");
					}
					else if(linha.trim().equals("ATHLETES"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM ATHLETES"))
						{
							List<String> atletas_local = Arrays.asList(linha.split("/<>/"));
							for (String atleta_local: atletas_local)
							{
								atleta = new Athlete();
								//String aux = "";
								atleta.atleta = (atleta_local.split("Atleta:")[1]).replace("'", "");
								//atleta.atleta = aux;
								//System.out.println("Atleta test:"+atleta.atleta);
								//atleta.atleta = aux.split("Link")[0];
								//System.out.println("Tipo Atleta:"+atleta.atleta);
								//atleta.link = aux.split("Link:")[1];
								//System.out.println(atleta.link);
								atleta.id_profile = profile.link;
								atleta.data_coleta = profile.data_coleta;

								profile.athletes.add(atleta);
							}
							linha = leitor.readLine();
						}
						System.out.println("Terminou Atleta");
					}
					else if(linha.trim().equals("MOVIES"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM MOVIES"))
						{
							List<String> movies_local = Arrays.asList(linha.split("/<>/"));
							for (String movie_local: movies_local)
							{
								movie = new Movie();
								//String aux = "";

								movie.movie = (movie_local.split("Movie:")[1]).replace("'", "");
								//movie.movie = (aux.split("Link:")[0]).replace("'", "");
								//movie.movie = aux;
								//System.out.println("Tipo Movie:"+movie.movie);
								//System.out.println(movie.movie);
								//movie.link = aux.split("Link:")[1];
								//System.out.println(movie.link);
								movie.id_profile = profile.link;
								movie.data_coleta = profile.data_coleta;

								profile.movies.add(movie);
							}
							linha = leitor.readLine();
						}
						System.out.println("Terminou Movie");
					}
					else if(linha.trim().equals("BOOKS"))
					{
						linha = leitor.readLine();
						while(!linha.trim().equals("FIM BOOKS"))
						{

							List<String> books_local = Arrays.asList(linha.split("/<>/"));
							for (String book_local: books_local)
							{
								book = new Book();
								//String aux = "";
								book.book = book_local.split("Book:")[1].replace("'", "");
								//book.book = aux.split("Link:")[0].replace("'", "");

								//book.book = aux;
								//System.out.println("Tipo Book:"+book.book);
								//book.link = aux.split("Link:")[1].replace("'", "");

								book.id_profile = profile.link;
								book.data_coleta = profile.data_coleta;


								profile.books.add(book);

							}
							linha = leitor.readLine();
							System.out.println("Terminou Book");
						}

					}
					else if(linha.trim().equals("GAMES"))
					{
						linha = leitor.readLine();
						if(linha.equals(""))
						{
							linha=leitor.readLine();
						}
						while(!linha.trim().equals("FIM GAMES"))
						{
							List<String> games_local = Arrays.asList(linha.split("/<>/"));
							for (String game_local: games_local)
							{
								game = new Game();
								//String aux = "";
								game.game = (game_local.split("Game:")[1]).replace("'", "");
								//game.game = aux;
								//game.game = aux.split("Link")[0];
								//System.out.println("Tipo Games:"+game.game);
								//game.link = aux.split("Link")[1];
								//System.out.println("Games = "+ game.game);
								game.id_profile = profile.link;
								game.data_coleta = profile.data_coleta;

								profile.games.add(game);
							}
							linha = leitor.readLine();
						}
						System.out.println("Terminou Game");
					}
					else if(linha.trim().equals("MUSICS"))
					{
						linha = leitor.readLine();
						if(linha.trim().equals(""))
						{
							linha= leitor.readLine();
						}
						while(!linha.trim().equals("FIM MUSICS"))
						{
							if(!linha.trim().equals(""))
							{
								//System.out.println("id:"+profile.link);
								List<String> musics_local = Arrays.asList(linha.split("/<>/"));
								for (String music_local: musics_local)
								{
									music =new Music();
									//System.out.println(linha);
									//System.out.println(music_local.split("Music:")[1]);
									//String aux = "";
									music.musicas = (music_local.split("Music:")[1]).replace("'", "");
									//music.musicas = (aux.split("Link:")[0]);
									//music.musicas = aux;
									//System.out.println("Tipo Music:"+music.musicas);
									//music.link = (aux.split("Link:")[1]);
									music.id_profile  = profile.link;
									music.data_coleta = profile.data_coleta;


									profile.musics.add(music);
								}
								linha = leitor.readLine();
							}
							else
							{
								linha = leitor.readLine();
							}

						}
						System.out.println("Terminou Musicas");
						linha =leitor.readLine();
					}
					else if(linha.trim().equals("LIKES"))
					{
						linha = leitor.readLine();

						while(!linha.trim().equals("FIM LIKES"))
						{
							//System.out.println(profile.link);
							//System.out.println(linha);
							if(linha.trim().equals("")){
								linha = "FIM LIKES";
							}
							else
							{
								String temp = linha.split("Curtiu:")[1];
								String curtida = temp.split("Tipo:")[0];
								String tipo = temp.split("Tipo:")[1].trim();
								//String aux = tipo;
								//String tipo_curtida = tipo.split("Link:")[0].trim();
								//String link = tipo.split("Link:")[1].trim();
								//System.out.println("curtida =" +link);
								likes = new Likes();
								likes.like = curtida;
								likes.tipo = tipo;
								//likes.tipo = tipo_curtida;
								//System.out.println("Tipo Likes:"+likes.tipo);
								//likes.link = link;
								likes.data_coleta = profile.data_coleta;
								//System.out.println(likes.tipo);
								profile.likes.add(likes);
								linha = leitor.readLine();

							}
						}
						System.out.println("Terminou Likes");
					}
					if(linha.equals("//--END PROFILE--//"))
					{
						profiles.profiles.add(profile);
						//String jsonWords1 = new Gson().toJson(profile);
						System.out.println(profile.link);
						linha=leitor.readLine();
					}
					else
					{
						if(!linha.trim().equals("LIKES"))
						{
							linha=leitor.readLine();
						}

					}
				}
				System.out.println("IdColeta: "+profile.id_collect);
				System.out.println("Data Coleta: "+profile.data_coleta);
				profile=null;
			}

		}
		Thread.sleep(100);

		System.out.println("ok");
		System.out.println(profiles.profiles.size());
			
			/*
			profiles.save();*/

	}


}



