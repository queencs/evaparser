package Util;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Model.Athlete;
import Model.Book;
import Model.Checkin;
import Model.Game;
import Model.Likes;
import Model.Movie;
import Model.Music;
import Model.Photo;
import Model.Profile;
import Model.Sport;
import Model.Tv;

public class InsertEvaCollect {

	public ArrayList<Profile> profiles = new ArrayList<Profile>(); 
	
	public void save() throws ClassNotFoundException, SQLException, IOException
	{
		Class.forName("com.mysql.jdbc.Driver");
		
		for (Profile profile : this.profiles) {
			Connection conexao = DriverManager.getConnection("jdbc:mysql://evaacollector.c8jcboubgzkm.sa-east-1.rds.amazonaws.com:3306/eva", "root", "Fag120dto");
			
			Statement comando = null;
			comando = conexao.createStatement();
			try{
				String sql = "INSERT INTO profile_profile(id_collect,id_profile, link, moradia, naturalidade, data_nascimento, genero, status_relacionamento, interesse, religiao, idiomas, data_coleta) "
						+ "VALUES('"+profile.id_collect+"','"+profile.id+"','"+profile.link+"','"+profile.moradia+"','"+profile.naturalidade+"','"+profile.data_nascimento+"','"+profile.genero+"','"+profile.relacionamento+"','"+profile.interesse+"','"+profile.religiao+"','"+profile.idiomas+"','"+profile.data_coleta+"');";
		
					
					System.out.println(sql);
					comando.execute(sql);
					for(Likes like : profile.likes)
					{
						
						sql = "INSERT INTO profile_likes(id_profile, curtida, tipo, data_coleta) "
								+ "VALUES('"+profile.link+"','"+like.like.replaceAll("'", "")+"', '"+like.tipo.replaceAll("'", "")+"', '"+like.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					
					}
					
					for(Tv tv : profile.tvs)
					{
						sql = "INSERT INTO profile_tv(id_profile, tv,data_coleta) "
								+ "VALUES('"+tv.id_profile+"','"+tv.tv.replaceAll("'", "")+"','"+tv.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
					for(Sport sport : profile.sports)
					{
						sql = "INSERT INTO profile_sports(id_profile, sport,data_coleta) "
								+ "VALUES('"+sport.id_profile+"','"+sport.sport.replaceAll("'", "")+"','"+sport.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
					
					for(Photo photo : profile.photos)
					{
						sql = "INSERT INTO profile_fotos(id_profile, descricao) "
						+ "VALUES('"+photo.id_profile+"','"+photo.descricao+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
					
					for(Music music : profile.musics)
					{
						System.out.println("Chegou aqui");
						sql = "INSERT INTO profile_musicas(id_profile, musicas,data_coleta) "
								+ "VALUES('"+music.id_profile+"','"+music.musicas.replaceAll("'", "")+"','"+music.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
					for(Movie movie :profile.movies)
					{
						sql = "INSERT INTO profile_filmes(id_profile, filme,data_coleta) "
								+ "VALUES('"+movie.id_profile+"','"+movie.movie.replaceAll("'", "")+"','"+movie.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
//					for(Familia familia : profile.membrosFamilia)
//					{
//						 sql = "INSERT INTO profile_familia(id_profile, nome, relacionamento,data_coleta) "
//									+ "	  VALUES('"+familia.id_profile+"','"+familia.nome+"', '"+familia.relacionamento+"', '"+familia.data_coleta+"');";
//						 System.out.println(sql);
//						comando.execute(sql);
//					}
					
					for(Game game :profile.games)
					{
						sql = "INSERT INTO profile_games(id_profile, game, data_coleta) "
								+ "VALUES('"+game.id_profile+"','"+game.game.replaceAll("'", "")+"','"+game.data_coleta+"');";
						System.out.println(sql);
							
						comando.execute(sql);
					}
					for(Checkin checkin : profile.checkins)
					{
						if(checkin.data == null || checkin.data.equals("null") )
						{
							sql = "INSERT INTO profile_checkins(id_profile, local,data_coleta) "
									+ "VALUES('"+checkin.id_profile+"','"+checkin.local.replaceAll("'", "")+"','"+checkin.data_coleta+"';";
							
						}
						else
						{
							sql = "INSERT INTO profile_checkins(id_profile, local, data,data_coleta) "
									+ "VALUES('"+checkin.id_profile+"','"+checkin.local.replaceAll("'", "").replaceAll("\"", "")+"', '"+checkin.data+"','"+checkin.data_coleta+"');";
							
						}
						System.out.println(sql);
						comando.execute(sql);
					}
					for(Book livro : profile.books)
					{
						sql = "INSERT INTO profile_livros(id_profile, livro,data_coleta) "
								+ "VALUES('"+livro.id_profile+"','"+livro.book.replaceAll("'", "")+"','"+livro.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
					for(Athlete athlete : profile.athletes)
					{
						sql = "INSERT INTO profile_atletas(id_profile, atleta,data_coleta) "
								+ "	  VALUES('"+athlete.id_profile+"','"+athlete.atleta.replaceAll("'", "")+"','"+athlete.data_coleta+"');";
						System.out.println(sql);
						comando.execute(sql);
					}
//					for(Acontecimento acontecimento: profile.acontecimentos)
//					{
//						sql = "INSERT INTO profile_acontecimentos(id_profile, acontecimento, ano) "
//								+ "	  VALUES('"+acontecimento.id_profile+"','"+acontecimento.acontecimento+"', '"+acontecimento.ano+"');";
//						System.out.println(sql);	
//						comando.execute(sql);
//					}
					
			}
			catch (Exception e) {
				FileWriter arquivo = new FileWriter("C:\\erro_inserir.txt", true);
				BufferedWriter buffer = new BufferedWriter(arquivo);
				buffer.write("erro: "+e+";\n Link: "+profile.link);
				buffer.newLine();
				buffer.write("-------------------------------------");
				buffer.newLine();
				buffer.flush();
				arquivo.close();
				continue;
			}			
			comando.close();
			conexao.close();
			
		}
		System.out.println("Update completo ");
	}
}
